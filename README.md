# MATIC (Polygon) Mainnet on MANTRA DAO

- Chain ID: `mainnet`

## Submodules

- [maticnetwork/bor](https://github.com/maticnetwork/bor.git) @ `v0.2.16`
- [maticnetwork/heimdall](https://github.com/maticnetwork/heimdall.git) @ `v0.2.9`
